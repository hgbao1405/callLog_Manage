import JsSIP from "jssip";
import NodeWebSocket from "../../entities/NodeWebSocket";
export default function makingCall_connected(
  { url, options, configuration },
  repoSip
) {
  console.log("makingCall:connected");
  const ws = NodeWebSocket({ url, options, configuration });
  return repoSip.makingCall_connected(ws);
}
