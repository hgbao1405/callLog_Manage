import callLogEntities from "../../../entities/callLog.js";
export default function createNcallog(list, repositoriesMongo) {
  const newcallLogs = new Array();

  list.forEach(({ name, email, phone_number, sex, duration }) => {
    const newcallLog = callLogEntities({
      name,
      email,
      phone_number,
      sex,
      duration,
    });
    if (!phone_number) throw new Error("Số điện thoại không thể trống");
    newcallLogs.push(newcallLog);
  });

  return repositoriesMongo.create_N(newcallLogs);
}
