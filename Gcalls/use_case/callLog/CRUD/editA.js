import callLogEntities from "../../../entities/callLog.js";
export default function editAcallog(
  id,
  { name, email, phone_number, sex, duration },
  repositoriesMongo
) {
  const newcallog = callLogEntities({
    name,
    email,
    phone_number,
    sex,
    duration,
  });
  return repositoriesMongo.update_A(id, newcallog);
}
