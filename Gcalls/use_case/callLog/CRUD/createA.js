import callLogEntities from "../../../entities/callLog.js";
export default function createAcallog(
  { name, email, phone_number, sex, duration },
  repositoriesMongo
) {
  if (!phone_number) throw new Error("Số điện thoại không thể trống");
  const newcallLog = callLogEntities({
    name,
    email,
    phone_number,
    sex,
    duration,
  });
  return repositoriesMongo.create_A(newcallLog);
}
