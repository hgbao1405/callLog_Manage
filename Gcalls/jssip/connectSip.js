import NodeWebSocket from "../entities/NodeWebSocket";

export default function connectJssip({ url, options }) {
  const isConnected = () => {
    const ws = NodeWebSocket({ url, options }).getWs();
    return ws && ws.readyState === ws.OPEN;
  };

  const isConnecting = () => {
    const ws = NodeWebSocket({ url, options }).getWs();
    return ws && ws.readyState === ws.CONNECTING;
  };

  const connect = () => {
    const ws = NodeWebSocket({ url, options }).getWs();
    if (isConnected()) {
      console.log("WebSocket already connected [url:" + url + "]");
      return;
    } else if (isConnecting()) {
      console.log("WebSocket already connecting [url:" + url + "]");
      return;
    }

    if (ws) ws.close();
    console.log("WebSocket connecting [url:" + url + "]");

    ws.onopen = () => {
      console.log("WebSocket open [url:" + url + "]");
      this.onconnect();
    };

    ws.onclose = (event) => {
      console.log(
        "WebSocket close [url:" +
          url +
          ",code:" +
          event.code +
          ",reason:" +
          event.reason +
          "]"
      );
      ondisconnect(event.wasClean, event.code, event.reason);
    };

    ws.onerror = () => {
      console.log('WebSocket error [url:"%s"]', url);
    };

    ws.onmessage = (event) => {
      console.log("WebSocket message received:(" + event.data + ")");
    };
  };
  const disconnect = () => {};
  {
    const ws = NodeWebSocket({ url, options }).getWs();
    debug("disconnect()");
    ws.close();
    ws = null;
  }
  return {
    connect,
    disconnect,
    isConnected,
    isConnecting,
  };
}
