import URL from "url";
import { w3cwebsocket } from "websocket";
export default function NodeWebSocket({ url, options, uri, password }) {
  return {
    getUri: () => uri,
    getUrl: () => url,
    getoptions: () => options,
    getPassword: () => password,
    getConfiguration: () => {
      return {
        sockets: [socket],
        uri: uri,
        password: password,
      };
    },
  };
}
