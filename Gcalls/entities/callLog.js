export default function callLog({
  id,
  name,
  email,
  phone_number,
  sex,
  duration,
}) {
  return {
    getId: () => id,
    getName: () => name,
    getEmail: () => email,
    getPhoneNumber: () => phone_number,
    getSex: () => sex,
    getduration: () => duration,
  };
}
