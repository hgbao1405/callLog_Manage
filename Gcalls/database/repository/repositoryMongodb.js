import callLogModel from "../model/callLog.js";
//Truy vấn mongodb
export default function mongodbRepo() {
  const show_A = (id) => {
    return callLogModel.findById(id);
  };
  const show_N = () => {
    console.log("showN mongodb repo");
    return callLogModel.find();
  };
  const create_A = (callLogEntities) => {
    const newCallog = new callLogModel({
      name: callLogEntities.getName(),
      email: callLogEntities.getEmail(),
      phone_number: callLogEntities.getPhoneNumber(),
      sex: callLogEntities.getSex(),
      duration: callLogEntities.getduration() ?? Date.now(),
    });
    return newCallog.save();
  };

  const create_N = (listcallLog) => {
    const list = new Array();
    listcallLog.forEach((callLogEntities) => {
      const newCallog = new callLogModel({
        name: callLogEntities.getName(),
        email: callLogEntities.getEmail(),
        phone_number: callLogEntities.getPhoneNumber(),
        sex: callLogEntities.getSex(),
        duration: callLogEntities.getduration() ?? Date.now(),
      });
      list.push(newCallog);
      newCallog.save();
    });
    return list;
  };
  const update_A = (id, callLogEntities) => {
    const old = callLogModel.findById(id);
    const res = callLogModel.replaceOne(
      { _id: id },
      {
        name: callLogEntities.getName() ?? old.name,
        email: callLogEntities.getEmail() ?? old.email,
        phone_number: callLogEntities.getPhoneNumber() ?? old.phone_number,
        sex: callLogEntities.getSex() ?? old.sex,
        duration: callLogEntities.getduration() ?? Date.now(),
      }
    );
    return res;
  };
  const update_N = (listcallLogWithId) => {
    listcallLogWithId.forEach((callLogEntities) => {
      const old = callLogModel.findById(callLogEntities.id);
      const res = callLogModel.replaceOne(
        { _id: id },
        {
          name: callLogEntities.getName() ?? old.name,
          email: callLogEntities.getEmail() ?? old.email,
          phone_number: callLogEntities.getPhoneNumber() ?? old.phone_number,
          sex: callLogEntities.getSex() ?? old.sex,
          duration: callLogEntities.getduration() ?? Date.now(),
        }
      );
    });
    return listcallLogWithId;
  };
  const delete_A = (id) => {
    return callLogModel.deleteOne({ id: id });
  };
  const delete_N = (ids) => {
    return callLogModel.find({ id: [ids] }).deleteMany();
  };
  return {
    show_A,
    show_N,
    create_A,
    create_N,
    update_A,
    update_N,
    delete_A,
    delete_N,
  };
}
