import mongoose, { Schema } from "mongoose";
import validator from "validator";
export default mongoose.model(
  "callLog",
  new Schema({
    id: Object,
    name: String,
    email: { type: String },
    phone_number: {
      type: String,
      required: true,
      validate: {
        validator: (value) => value.length >= 10,
        message: "Số điện thoại phải lớn hơn 9 số",
      },
    },
    sex: { type: String, enum: ["Nam", "Nữ"] },
    duration: { type: Date, default: Date.now() },
  })
);
