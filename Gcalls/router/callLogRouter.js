import Controller from "../cotroller/callogController.js";
import callLogRepo from "../repository/callLogRepo.js";
import repositoriDb from "../database/repository/repositoryMongodb.js";
export default function Router(app, express) {
  const router = express.Router();
  const controller = Controller(callLogRepo, repositoriDb);

  router.get("/", controller.showN);
  router.get("/:id", controller.showA);

  router.post("/", controller.createA);
  router.post("/CreateMany", controller.createN);

  router.put("/:id", controller.updateA);
  router.put("/Update", controller.updateN);

  router.delete("/:id", controller.deleteA);
  router.delete("/delete", controller.deleteN);

  app.use("/callLog", router);
}
