import express from "express";
import callLogRouter from "./router/callLogRouter.js";
import connect from "./database/connectionMongo.js";
import mongoose from "mongoose";
import * as dotenv from "dotenv";

dotenv.config();
const app = express();
const port = 3000;

app.use(express.json());

app.get("/", (req, res) => {
  res.send("this is homepage");
});

callLogRouter(app, express);

app.listen(port, async () => {
  await connect(mongoose, process.env.MONGOURL);
  console.log("listening on port: http://localhost:" + port);
});
