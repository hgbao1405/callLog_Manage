import createAcallog from "../use_case/callLog/CRUD/createA.js";
import createNcaLLog from "../use_case/callLog/CRUD/createN.js";
import showNcallog from "../use_case/callLog/CRUD/showN.js";
import showAcallog from "../use_case/callLog/CRUD/showA.js";
import updateAcallog from "../use_case/callLog/CRUD/editA.js";
import updateNcallog from "../use_case/callLog/CRUD/editN.js";
import deleteAcallog from "../use_case/callLog/CRUD/deleteA.js";
import deleteNcallog from "../use_case/callLog/CRUD/deteteN.js";
export default function controller(callLogRepository, repositoriDb) {
  const repository = callLogRepository(repositoriDb());
  //Xong
  const showA = (req, res) => {
    const id = req.params.id;
    console.log(req.params);
    showAcallog(id, repository).then((item) => {
      res.status(200).json({
        data: item,
      });
    });
  };
  //Xong
  const showN = (req, res) => {
    console.log("show N controller");
    showNcallog(repository).then((list) => {
      res.status(200).json({
        message: "",
        data: list,
      });
    });
  };
  //Xong
  const createA = (req, res) => {
    const { name, email, phone_number, sex, duration } = req.body;
    createAcallog({ name, email, phone_number, sex, duration }, repository)
      .then(() => {
        res.status(200).json({
          message: "Thêm call Log thành công",
        });
      })
      .catch((err) => {
        res.status(404).json({
          message: err.message,
        });
      });
  };
  //Xong
  const createN = (req, res) => {
    console.log(req.body);
    req.body.forEach((item) => {
      const { name, email, phone_number, sex, duration } = item;
    });
    try {
      const result = createNcaLLog(req.body, repository);
      res.status(200).json({
        message: "Tạo nhiều call Log thành công",
        data: result,
      });
    } catch (err) {
      throw new Error("Lỗi");
    }
  };
  const updateA = (req, res) => {
    const { id, name, email, phone_number, sex, duration } = req.body;
    updateAcallog(req.body, repository)
      .then(() => {
        res.status(200).json({
          message: "Cập nhật call Log thành công",
        });
      })
      .catch((err) => {
        res.status(200).json({
          message: "Cập nhật call Log thất bại",
        });
      });
  };
  const updateN = (req, res) => {
    req.body.forEach((item) => {
      const { id, name, email, phone_number, sex, duration } = item;
    });
    updateNcallog(id, req.body, repository)
      .then(() => {
        res.status(200).json({
          message: "Cập nhật call Log thành công",
        });
      })
      .catch((err) => {
        res.status(200).json({
          message: "Cập nhật call Log thất bại",
        });
      });
    res.send(JSON.stringify(repository.update_N(req.body)));
  };
  const deleteA = (req, res) => {
    const id = req.params.id;
    deleteAcallog(id, repository)
      .then(() => {
        res.status(200).json({
          message: "Xóa call Log thành công",
        });
      })
      .catch((err) => {
        res.status(200).json({
          message: "Xóa call Log thất bại",
        });
      });
  };
  const deleteN = (req, res) => {
    deleteNcallog(req.body, repository)
      .then(() => {
        res.status(200).json({
          message: "Xóa call Log thành công",
        });
      })
      .catch((err) => {
        res.status(200).json({
          message: "Xóa call Log thất bại",
        });
      });
  };
  return {
    showA,
    showN,
    createA,
    createN,
    updateA,
    updateN,
    deleteA,
    deleteN,
  };
}
