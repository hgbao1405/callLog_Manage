export default function callLogRepo(repositoryMongo) {
  const show_A = (id) => {
    return repositoryMongo.show_A(id);
  };
  const show_N = () => {
    return repositoryMongo.show_N();
  };
  const create_A = (callLog) => {
    console.log(callLog);
    return repositoryMongo.create_A(callLog);
  };
  const create_N = (listcallLog) => {
    return repositoryMongo.create_N(listcallLog);
  };
  const update_A = (id, callog) => {
    return repositoryMongo.update_A(id, callog);
  };
  const update_N = () => {
    return repositoryMongo.update_N();
  };
  const delete_A = () => {
    return repositoryMongo.delete_A();
  };
  const delete_N = () => {
    return repositoryMongo.delete_N();
  };
  return {
    show_A,
    show_N,
    create_A,
    create_N,
    update_A,
    update_N,
    delete_A,
    delete_N,
  };
}
