import entityWS from "../entities/NodeWebSocket";
import JsSIP from "jssip";
export default function jsSipRepo() {
  const makingCall_busy = () => makingCall_busy();
  const makingCall_connected = (entityWS) => {
    var socket = new JsSIP.WebSocketInterface(entityWS.getUrl());
    var configuration = entityWS.getConfiguration();
    var ua = new JsSIP.UA(configuration);

    var options = {
      eventHandlers: eventHandlers,
      mediaConstraints: { audio: true, video: true },
    };
    var session = ua.call("sip:bob@example.com", options);
    return session;
  };

  return { makingCall_connected };
}
